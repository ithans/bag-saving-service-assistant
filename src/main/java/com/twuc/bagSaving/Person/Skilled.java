package com.twuc.bagSaving.Person;

import com.twuc.bagSaving.Bag;
import com.twuc.bagSaving.Cabinet;
import com.twuc.bagSaving.LockerSize;
import com.twuc.bagSaving.Ticket;

public class Skilled implements BagManager {
    //story9
    private final Cabinet[] cabinets;

    public Skilled(Cabinet ...cabinets) {
        this.cabinets=cabinets;
    }

    public Ticket saveBag(Bag bag){
        for (Cabinet cabinet :cabinets){
            try {
                Ticket ticket = cabinet.save(bag, LockerSize.SMALL);
                return ticket;
            }catch (Exception e){}
        }
        for (Cabinet cabinet :cabinets){
            try {
                Ticket ticket = cabinet.save(bag, LockerSize.MEDIUM);
                return ticket;
            }catch (Exception e){}
        }
        for (Cabinet cabinet :cabinets){
            try {
                Ticket ticket = cabinet.save(bag, LockerSize.BIG);
                return ticket;
            }catch (Exception e){}
        }
        return null;
    }
    public Bag getBag(Ticket ticket){
        for (Cabinet cabinet:cabinets){
            try {
                Bag bag = cabinet.getBag(ticket);
                return bag;
            }catch (Exception e){}
        }
        return null;
    }


}
