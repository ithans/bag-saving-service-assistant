package com.twuc.bagSaving.Person;

import com.twuc.bagSaving.Bag;
import com.twuc.bagSaving.Cabinet;
import com.twuc.bagSaving.LockerSize;
import com.twuc.bagSaving.Ticket;

public class Foolish implements BagManager {
    //story8
    private final Cabinet[] cabinet;

    public Foolish(Cabinet ...cabinet) {
        this.cabinet = cabinet;
    }

    public Ticket save(Bag bag) {
        Ticket ticket;
        for (Cabinet cabinet1 :cabinet){
            try{
                ticket = cabinet1.save(bag, LockerSize.SMALL);
                return ticket;
            }catch (Exception e){}
            try{
                ticket = cabinet1.save(bag, LockerSize.MEDIUM);
                return ticket;
            }catch (Exception e){}
            try{
                ticket = cabinet1.save(bag, LockerSize.BIG);
                return ticket;
            }catch (Exception e){}
        }
        return null;
    }


    public Bag getBag(Ticket ticket) {
        for (Cabinet cabinet1 :cabinet){
            try {
            Bag bag = cabinet1.getBag(ticket);
            return bag;
            }catch (Exception e){

            }
        }
        return null;
    }
}
