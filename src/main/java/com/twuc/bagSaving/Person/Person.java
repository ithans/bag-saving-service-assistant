package com.twuc.bagSaving.Person;

import com.twuc.bagSaving.*;

public class Person implements BagManager{
    //story6
    public Ticket save(Bag bag, Cabinet cabinet) {
        if (bag.getBagSize()== BagSize.SMALL){
            return cabinet.save(bag, LockerSize.SMALL);
        }
        if (bag.getBagSize()== BagSize.MEDIUM){
            return cabinet.save(bag, LockerSize.MEDIUM);
        }
        if (bag.getBagSize()== BagSize.BIG){
            return cabinet.save(bag, LockerSize.BIG);
        }
        return null;
    }

    public Bag getBag(Ticket ticket,Cabinet cabinet){
        return cabinet.getBag(ticket);
    }
}
