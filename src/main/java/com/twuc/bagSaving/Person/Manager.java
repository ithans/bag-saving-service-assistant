package com.twuc.bagSaving.Person;

import com.twuc.bagSaving.Bag;
import com.twuc.bagSaving.Ticket;

public class Manager {
    public Ticket save(Skilled skilled, Bag bag) {
        Ticket ticket = skilled.saveBag(bag);
        return ticket;
    }

    public Bag getBag(Skilled skilled, Ticket ticket) {
        return skilled.getBag(ticket);
    }
}
