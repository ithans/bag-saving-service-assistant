package com.twuc.bagSaving;

import com.twuc.bagSaving.Person.Foolish;
import com.twuc.bagSaving.Person.Manager;
import com.twuc.bagSaving.Person.Person;
import com.twuc.bagSaving.Person.Skilled;
import org.junit.jupiter.api.Test;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

public class PersonTest {

    @Test
    void should_save_bag_by_people() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Person person = new Person();
        Ticket ticket = person.save(new Bag(BagSize.BIG), cabinet1);
        assertNotNull(ticket);
    }

    @Test
    void should_not_sava_package() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.SMALL, 1));
        Person person = new Person();
        assertThrows(InsufficientLockersException.class, () -> {
            person.save(new Bag(BagSize.BIG), cabinet1);
        });
    }

    @Test
    void should_get_a_bag() {
        Cabinet cabinet = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Person person = new Person();
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = person.save(bag, cabinet);
        Bag fetchedBag = person.getBag(ticket, cabinet);
        assertSame(bag, fetchedBag);
    }

    @Test
    void save_bag_by_foolish() {
        Cabinet cabinet1 = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet2 = createCabinetWithPlentyOfCapacity();
        Foolish foilish = new Foolish(cabinet1,cabinet2);
        Bag bag =new Bag(BagSize.SMALL);
        Ticket ticket = foilish.save(bag);
        assertNotNull(ticket);
    }
    @Test
    void get_bag_by_foolish() {
        Cabinet cabinet1 = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet2 = createCabinetWithPlentyOfCapacity();
        Foolish foilish1 = new Foolish(cabinet1,cabinet2);
        Foolish foilish2 = new Foolish(cabinet1,cabinet2);
        Bag bag =new Bag(BagSize.SMALL);
        Ticket ticket = foilish1.save(bag);
        Bag back = foilish2.getBag(ticket);
        assertEquals(bag,back);
    }

    @Test
    void should_save_by_skill() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Cabinet cabinet2 = new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1));
        Skilled skilled1 = new Skilled(cabinet1,cabinet2);
        Skilled skilled2 = new Skilled(cabinet1,cabinet2);
        Ticket ticket = skilled1.saveBag(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }
    @Test
    void should_save_by_skill_error() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Cabinet cabinet2 = new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1));
        Skilled skilled1 = new Skilled(cabinet1,cabinet2);
        Skilled skilled2 = new Skilled(cabinet1,cabinet2);
        Ticket ticket = skilled1.saveBag(new Bag(BagSize.MINI));
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Cabinet cabinet2 = new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1));
        Skilled skilled1 = new Skilled(cabinet1,cabinet2);
        Skilled skilled2 = new Skilled(cabinet1,cabinet2);
        Bag bag1 = new Bag(BagSize.MEDIUM);
        Ticket ticket = skilled1.saveBag(bag1);
        Bag bag = skilled2.getBag(ticket);
        assertEquals(bag1,bag);
    }

    @Test
    void should_save_bag_by_manager() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Cabinet cabinet2 = new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1));
        Manager manager = new Manager();
        Skilled skilled =new Skilled(cabinet1,cabinet2);
        Bag bag = new Bag(BagSize.SMALL);
        Ticket ticket = manager.save(skilled,bag);
        assertNotNull(ticket);
    }
    @Test
    void should_get_bag_by_manager() {
        Cabinet cabinet1 = new Cabinet(LockerSetting.of(LockerSize.BIG, 1));
        Cabinet cabinet2 = new Cabinet(LockerSetting.of(LockerSize.MEDIUM, 1));
        Manager manager = new Manager();
        Skilled skilled =new Skilled(cabinet1,cabinet2);
        Bag bag = new Bag(BagSize.SMALL);
        Ticket ticket = manager.save(skilled,bag);

        Bag bag1 = manager.getBag(skilled,ticket);
        assertEquals(bag,bag1);
    }
}
